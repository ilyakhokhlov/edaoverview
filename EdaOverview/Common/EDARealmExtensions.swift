//
//  EDARealmExtensions.swift
//  EdaOverview
//
//  Created by Ilya on 13.07.2018.
//  Copyright © 2018 Ilya Khokhlov. All rights reserved.
//

import RealmSwift

public extension Realm {
    
    public func safeWrite(_ block: @escaping () throws -> Void) throws {
        let needBeginTransaction = !self.isInWriteTransaction
        if needBeginTransaction {
            self.beginWrite()
        }
        try block()
        if needBeginTransaction {
            try self.commitWrite()
        }
    }
}
