//
//  EDADataError.swift
//  EdaOverview
//
//  Created by Ilya on 13.07.2018.
//  Copyright © 2018 Ilya Khokhlov. All rights reserved.
//

import UIKit

enum EDADataError: Error {
    case invalidData(String)
}
