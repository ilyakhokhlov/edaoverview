//
//  EDAAPIManager.swift
//  EdaOverview
//
//  Created by Ilya on 13.07.2018.
//  Copyright © 2018 Ilya Khokhlov. All rights reserved.
//

import UIKit
import Alamofire

class EDAAPIManager: NSObject {
    fileprivate static let backgroundQueue = DispatchQueue.global(qos: .background)
    
    static func downloadPlaces(_ completion: @escaping (Error?)->()) {
        Alamofire.request("https://eda.yandex/api/v2/catalog?latitude=55.762885&longitude=37.597360").responseJSON(queue: backgroundQueue) { (response) in
            var finalError: Error?
            defer {
                DispatchQueue.main.async {
                    completion(finalError)
                }
            }
            if response.result.isSuccess {
                let result = response.result.value as? [String: Any]
                let items = (result?["payload"] as? [String: Any])?["foundPlaces"] as? [[String: Any]]
                do {
                    try EDAPlace.saveObjects(fromJsonArray: items)
                } catch let error {
                    finalError = error
                }
            } else {
                finalError = response.error
            }
        }
    }
}
