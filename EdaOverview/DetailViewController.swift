//
//  DetailViewController.swift
//  EdaOverview
//
//  Created by Ilya on 12.07.2018.
//  Copyright © 2018 Ilya Khokhlov. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var detailDescriptionLabel: UILabel?
    @IBOutlet weak var imageView: UIImageView?

    func configureView() {
        // Update the user interface for the detail item.
        if let detail = detailItem {
            self.detailDescriptionLabel?.text = detail.placeDescription
            EDAImageManager.shared().setupImage(fromUrl: detail.imageUrl(width: 100, height: 75), toImageView: self.imageView)
            self.title = detail.name
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        EDAImageManager.shared().addObserver(self)
        self.detailDescriptionLabel?.font = UIFont.preferredFont(forTextStyle: .body)
        self.detailDescriptionLabel?.numberOfLines = 0
        self.imageView?.contentMode = .scaleAspectFill
        configureView()
    }
    
    deinit {
        EDAImageManager.shared().removeObserver(self)
    }

    var detailItem: EDAPlace? {
        didSet {
            // Update the view.
            configureView()
        }
    }


}

extension DetailViewController: EDAImageManagerObserver {
    func imageManagerProviderChanged(_ imageManager: EDAImageManager) {
        configureView()
    }
    
    func imageManager(_ imageManager: EDAImageManager, cacheClearedForProvider provider: EDAImageProvider) {
        if provider == EDAImageManager.shared().currentImageProvider {
            configureView()
        }
    }
}

