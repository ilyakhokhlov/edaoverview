//
//  MasterViewController.swift
//  EdaOverview
//
//  Created by Ilya on 12.07.2018.
//  Copyright © 2018 Ilya Khokhlov. All rights reserved.
//

import UIKit
import RealmSwift

class MasterViewController: UITableViewController {

    var detailViewController: DetailViewController? = nil
    var objects: Results<EDAPlace>?
    var objectsToken: NotificationToken? = nil

    fileprivate lazy var imageProviderButton: UIBarButtonItem = {
        [unowned self] in
        let result = UIBarButtonItem(image: UIImage(named: "iconChooseImageProvider"), style: .plain, target: self, action: #selector(self.imageProviderButtonTapped))
        return result
    }()
    
    fileprivate lazy var imageCacheButton: UIBarButtonItem = {
        [unowned self] in
        let result = UIBarButtonItem(image: UIImage(named: "iconClearImageCache"), style: .plain, target: self, action: #selector(self.imageCacheButtonTapped))
        return result
        }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        EDAImageManager.shared().addObserver(self)
        
        self.title = "Где поесть?"

        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
        
        let refreshControl = UIRefreshControl(frame: .zero)
        refreshControl.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
        tableView.refreshControl = refreshControl
        
        tableView.register(UINib(nibName: "EDAPlaceTableViewCell", bundle: nil), forCellReuseIdentifier: String(describing: EDAPlaceTableViewCell.self))
        
        let realm = try? Realm()
        self.objects = realm?.objects(EDAPlace.self)
        self.objectsToken = self.objects?.observe({[weak self] (changes) in
            guard let tableView = self?.tableView else { return }
            switch changes {
            case .initial:
                // Results are now populated and can be accessed without blocking the UI
                tableView.reloadData()
            case .update(_, let deletions, let insertions, let modifications):
                // Query results have changed, so apply them to the UITableView
                tableView.beginUpdates()
                tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.deleteRows(at: deletions.map({ IndexPath(row: $0, section: 0)}),
                                     with: .automatic)
                tableView.reloadRows(at: modifications.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
                tableView.endUpdates()
            case .error(let error):
                // An error occurred while opening the Realm file on the background worker thread
                fatalError("\(error)")
            }
        })
        self.navigationItem.leftBarButtonItem = self.imageCacheButton
        self.navigationItem.rightBarButtonItem = self.imageProviderButton
        self.updateData()
    }
    
    deinit {
        EDAImageManager.shared().removeObserver(self)
        self.objectsToken?.invalidate()
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }
    
    // MARK: - Actions
    
    @objc func refresh() {
        self.updateData()
    }
    
    @objc func imageProviderButtonTapped() {
        let alert = UIAlertController(title: nil, message: "Выберите библиотеку для загрузки изображений", preferredStyle: .actionSheet)
        EDAImageProvider.allValues().forEach { (provider) in
            var title = provider.title()
            if provider == EDAImageManager.shared().currentImageProvider {
                title.append(" \u{2713}")
            }
            let providerAction = UIAlertAction(title: title, style: .default, handler: { (action) in
                EDAImageManager.shared().currentImageProvider = provider
            })
            alert.addAction(providerAction)
        }
        
        let cancelAction = UIAlertAction(title: "Отменить", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        alert.popoverPresentationController?.barButtonItem = self.imageProviderButton
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func imageCacheButtonTapped() {
        let alert = UIAlertController(title: nil, message: "Очистить кэш изображений для:", preferredStyle: .actionSheet)
        
        let providerAction = UIAlertAction(title: EDAImageManager.shared().currentImageProvider.title(), style: .default, handler: {(action) in
            EDAImageManager.shared().clearCache(provider: EDAImageManager.shared().currentImageProvider)
        })
        alert.addAction(providerAction)
        
        let allAction = UIAlertAction(title: "Всех библиотек", style: .default, handler: {(action) in
            EDAImageManager.shared().clearAllCache()
        })
        alert.addAction(allAction)
        
        let cancelAction = UIAlertAction(title: "Отменить", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        alert.popoverPresentationController?.barButtonItem = self.imageCacheButton
        self.present(alert, animated: true, completion: nil)
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let object = objects?[indexPath.row]
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = object
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }
    
    // MARK: - Objects
    
    fileprivate func updateData() {
        self.refreshControl?.beginRefreshing()
        EDAAPIManager.downloadPlaces {[weak self] (error) in
            self?.refreshControl?.endRefreshing()
            if error != nil {
                self?.showErrorAlert()
            }
        }
    }
    
    // MARK: - Alert
    
    fileprivate func showErrorAlert() {
        let alert = UIAlertController(title: "Ошибка", message: "Произошло что-то странное при загрузке мест", preferredStyle: .alert)
        let retryAction = UIAlertAction(title: "Повторить", style: .default, handler: {[weak self] (action) in
            self?.updateData()
        })
        alert.addAction(retryAction)
        let cancelAction = UIAlertAction(title: "Отменить", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: EDAPlaceTableViewCell.self), for: indexPath) as! EDAPlaceTableViewCell

        let object = objects?[indexPath.row]
        cell.placeTitleLabel?.text = object?.name
        cell.placeDescriptionLabel?.text = object?.placeDescription
        EDAImageManager.shared().setupImage(fromUrl: object?.imageUrl(width: 100, height: 75), toImageView: cell.placeImageView)
        
        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showDetail", sender: nil)
    }

}

extension MasterViewController: EDAImageManagerObserver {
    func imageManagerProviderChanged(_ imageManager: EDAImageManager) {
        self.tableView.reloadData()
    }
    
    func imageManager(_ imageManager: EDAImageManager, cacheClearedForProvider provider: EDAImageProvider) {
        if provider == EDAImageManager.shared().currentImageProvider {
            self.tableView.reloadData()
        }
    }
}

