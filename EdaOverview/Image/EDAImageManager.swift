//
//  EDAImageManager.swift
//  EdaOverview
//
//  Created by Ilya on 15.07.2018.
//  Copyright © 2018 Ilya Khokhlov. All rights reserved.
//

import UIKit
import Kingfisher
import Nuke
import SDWebImage

enum EDAImageProvider: Int {
    case sdWebImage = 1
    case kingfisher
    case nuke
    
    static func allValues() -> [EDAImageProvider] {
        return [.sdWebImage, .kingfisher, .nuke]
    }
    
    func title() -> String {
        switch self {
        case .sdWebImage:
            return "SDWebImage"
        case .kingfisher:
            return "Kingfisher"
        case .nuke:
            return "Nuke"
        }
    }
}

protocol EDAImageManagerObserver: NSObjectProtocol {
    func imageManagerProviderChanged(_ imageManager: EDAImageManager)
    func imageManager(_ imageManager: EDAImageManager, cacheClearedForProvider provider: EDAImageProvider)
}

class EDAImageManager: NSObject {
    
    // MARK: - Singletone
    
    private static var imageManager: EDAImageManager = {
        let imageManager = EDAImageManager()
        return imageManager
    }()
    
    private override init() {
        
    }
    
    class func shared() -> EDAImageManager {
        return imageManager
    }
    
    // MARK: - Observing
    
    func addObserver(_ observer:EDAImageManagerObserver) {
        self.observers.add(observer)
    }
    
    func removeObserver(_ observer:EDAImageManagerObserver) {
        self.observers.remove(observer)
    }
    
    fileprivate var observers:NSHashTable<AnyObject> = NSHashTable(options: .weakMemory)
    
    fileprivate func observeImageProviderChanged() {
        for observer in self.observers.allObjects {
            if let imageManagerObserver = observer as? EDAImageManagerObserver {
                imageManagerObserver.imageManagerProviderChanged(self)
            }
        }
    }
    
    fileprivate func observeCacheCleared(forProvider provider: EDAImageProvider) {
        for observer in self.observers.allObjects {
            if let imageManagerObserver = observer as? EDAImageManagerObserver {
                imageManagerObserver.imageManager(self, cacheClearedForProvider: provider)
            }
        }
    }
    
    // MARK: - Properties
    
    var currentImageProvider: EDAImageProvider {
        get {
            let savedValue = UserDefaults.standard.integer(forKey: "EDAImageManager_imageProvider")
            return EDAImageProvider(rawValue: savedValue) ?? .sdWebImage
        }
        set {
            UserDefaults.standard.set(newValue.rawValue, forKey: "EDAImageManager_imageProvider")
            self.observeImageProviderChanged()
        }
    }
    
    // MARK: - Images
    
    func setupImage(fromUrl: URL?, toImageView: UIImageView?) {
        guard let url = fromUrl, let imageView = toImageView else {
            toImageView?.image = nil
            return
        }
        switch self.currentImageProvider {
        case .sdWebImage:
            imageView.sd_setImage(with: url)
        case .kingfisher:
            imageView.kf.setImage(with: url)
        case .nuke:
            Nuke.loadImage(with: url, into: imageView)
        }
    }
    
    // MARK: - Cache
    
    func clearCache(provider: EDAImageProvider) {
        switch provider {
        case .sdWebImage:
            SDImageCache.shared().clearMemory()
            SDImageCache.shared().clearDisk()
        case .kingfisher:
            ImageCache.default.clearMemoryCache()
            ImageCache.default.clearDiskCache()
            ImageCache.default.cleanExpiredDiskCache()
        case .nuke:
            ImageCache.shared.removeAll()
            DataLoader.sharedUrlCache.removeAllCachedResponses()
        }
        self.observeCacheCleared(forProvider: provider)
    }
    
    func clearAllCache() {
        EDAImageProvider.allValues().forEach {
            self.clearCache(provider: $0)
        }
    }
}
