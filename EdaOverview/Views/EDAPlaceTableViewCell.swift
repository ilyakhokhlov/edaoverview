//
//  EDAPlaceTableViewCell.swift
//  EdaOverview
//
//  Created by Ilya on 15.07.2018.
//  Copyright © 2018 Ilya Khokhlov. All rights reserved.
//

import UIKit

class EDAPlaceTableViewCell: UITableViewCell {
    @IBOutlet weak var placeImageView: UIImageView?
    @IBOutlet weak var placeTitleLabel: UILabel?
    @IBOutlet weak var placeDescriptionLabel: UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.placeTitleLabel?.font = UIFont.preferredFont(forTextStyle: .body)
        self.placeTitleLabel?.numberOfLines = 0
        self.placeDescriptionLabel?.font = UIFont.preferredFont(forTextStyle: .caption1)
        self.placeDescriptionLabel?.numberOfLines = 0
        self.placeImageView?.contentMode = .scaleAspectFill
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
