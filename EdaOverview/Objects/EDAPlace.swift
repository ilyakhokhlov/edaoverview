//
//  EDAPlace.swift
//  EdaOverview
//
//  Created by Ilya on 13.07.2018.
//  Copyright © 2018 Ilya Khokhlov. All rights reserved.
//

import RealmSwift
import Foundation

class EDAPlace: Object {
    let id = RealmOptional<Int64>()
    
    @objc dynamic var name: String?
    @objc dynamic var placeDescription: String?
    @objc dynamic var thumbnailUrl: String?
    
    override open static func primaryKey() -> String? {
        return "id"
    }
}

extension EDAPlace {
    @discardableResult
    static func saveObject(fromJson json: [String: Any], realm aRealm: Realm? = nil) throws -> EDAPlace {
        guard let place = json["place"] as? [String: Any], let id = place["id"] as? Int64 else { throw EDADataError.invalidData("Couldn't save EDAPlace object with empty `id`")}
        let realm: Realm
        if let aRealm = aRealm {
            realm = aRealm
        } else {
            realm = try Realm()
        }
        var object: EDAPlace!
        try realm.safeWrite {
            object = realm.create(EDAPlace.self, value: ["id": id], update: true)
            object.name = place["name"] as? String
            object.placeDescription = place["description"] as? String
            object.thumbnailUrl = (place as NSDictionary).value(forKeyPath: "picture.uri") as? String
        }
        return object
    }
    
    @discardableResult
    static func saveObjects(fromJsonArray array: [[String: Any]]?, realm aRealm: Realm? = nil) throws -> [EDAPlace] {
        var results: [EDAPlace] = []
        guard let array = array, array.count > 0 else { return results }
        let realm: Realm
        if let aRealm = aRealm {
            realm = aRealm
        } else {
            realm = try Realm()
        }
        try realm.safeWrite {
            for item in array {
                let object = try self.saveObject(fromJson: item, realm: realm)
                results.append(object)
            }
        }
        return results
    }
}

extension EDAPlace {
    func imageUrl(width: Int, height: Int) -> URL? {
        let imageAddress = self.thumbnailUrl?.replacingOccurrences(of: "{w}", with: "\(width)").replacingOccurrences(of: "{h}", with: "\(height)") ?? ""
        let resultAddress = "http://eda.yandex" + imageAddress
        return URL(string: resultAddress)
    }
}
